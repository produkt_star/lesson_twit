
from flask import Flask, jsonify, request

app = Flask(__name__)

# Создадим начальные данные для пользователей и постов
users = [
    {"id": 1, "username": "user1"},
    {"id": 2, "username": "user2"}
]

posts = [
    {"id": 1, "title": "Post 1", "content": "Content 1"},
    {"id": 2, "title": "Post 2", "content": "Content 2"}
]

# Создаем запрос для получения всех пользователей используя localhost:5000/users
@app.route('/users', methods=['GET'])
def get_users():
    return jsonify(users)

# Создаем запрос для создания пользователя используя localhost:5000/users
@app.route('/users', methods=['POST'])
def create_user():
    new_user = request.json
    users.append(new_user)
    return jsonify(new_user)

# Используем для создания тело {"id": 3, "username": "user3"} и так далее...

# Создаем запрос для получения всех постов используя localhost:5000/posts
@app.route('/posts', methods=['GET'])
def get_posts():
    return jsonify(posts)

# Создаем запрос для создания поста используя localhost:5000/posts
@app.route('/posts', methods=['POST'])
def create_post():
    new_post = request.json
    posts.append(new_post)
    return jsonify(new_post)

# Используем для создания тело {"id": 3, "title": "Post 3", "content": "Content 3"} и так далее...

# Создаем запрос для изменения поста по его id
@app.route('/posts/<int:post_id>', methods=['PUT'])
def update_post(post_id):
    for post in posts:
        if post['id'] == post_id:
            post['title'] = request.json.get('title', post['title'])
            post['content'] = request.json.get('content', post['content'])
            return jsonify(post)
    return jsonify({'error': 'Пост не найден'}), 404

# Для изменения илпользуем параметр localhost:5000/posts/:id с условием value с нужным id
# используем тело {"id": 4, "title": "Post 4", "content": "Posts 4"} 
# в котором прописыаем нужные параметры для изменения

# Создаем запрос для удаления поста по его id
@app.route('/posts/<int:post_id>', methods=['DELETE'])
def delete_post(post_id):
    for index, post in enumerate(posts):
        if post['id'] == post_id:
            deleted_post = posts.pop(index)
            return jsonify(deleted_post)
    return jsonify({'error': 'Пост не найден'}), 404

# Для удаления илпользуем параметр localhost:5000/posts/:id с условием value с нужным id
# в котором прописыаем нужные для удаления id, тело не спользуем.

if __name__ == '__main__':
    app.run()
